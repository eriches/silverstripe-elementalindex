<?php

namespace Hestec\ElementalExtensions\Dataobjects;

use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use Hestec\ElementalExtensions\Elements\ElementIndex;
use SilverStripe\Forms\FieldList;
use SilverStripe\Security\Permission;
use SilverStripe\Forms\OptionsetField;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\RequiredFields;

class Chapter extends DataObject {

    private static $table_name = 'HestecElementChapter';

    private static $singular_name = 'Chapter';
    private static $plural_name = 'Chapters';

    private static $db = [
        'Title' => 'Varchar(255)',
        'Anchor' => 'Varchar(255)',
        'Level' => "Enum('1,2,3','1')",
        'Sort' => 'Int'
    ];

    private static $defaults = array(
        'Level' => 1
    );

    private static $has_one = [
        'ElementIndex' => ElementIndex::class
    ];

    private static $summary_fields = [
        'Title' => 'Title',
        'Anchor' => 'Anchor',
        'Level' => 'Level'
    ];

    private static $default_sort = 'Sort';

    public function getCMSFields()
    {
        $TitleField = TextField::create('Title', _t('Element.TITLE', "Title"));
        $AnchorField = TextField::create('Anchor', _t('ElementIndex.ANCHOR_TAG', "Anchor tag"));
        $AnchorField->setDescription(_t('ElementIndex.ANCHOR_TAG_DESCRIPTION', "If you leave this field empty, the title will be made an anchor tag."));
        $LevelField = OptionsetField::create('Level', "Level", $this->dbObject('Level')->enumValues());
        $LevelField->addExtraClass("layout-horizontal");

        return new FieldList(
            $TitleField,
            $AnchorField,
            $LevelField
        );

    }

    public function getCMSValidator() {

        return new RequiredFields(array(
            'Title'
        ));
    }

    public function onBeforeWrite() {

        if (strlen($this->Anchor) < 2){

            $this->Anchor = $this->Title;

        }

        $this->Anchor = SiteTree::create()->generateURLSegment($this->Anchor);

        parent::onBeforeWrite();

    }

    public function canView($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canEdit($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canDelete($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canCreate($member = null, $context = [])
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

}
