<?php

namespace Hestec\ElementalExtensions\Elements;

use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use Hestec\ElementalExtensions\Dataobjects\Chapter;
use SilverStripe\Forms\TextareaField;

class ElementIndex extends BaseElement
{

    private static $table_name = 'HestecElementIndex';

    private static $singular_name = 'Index';

    private static $plural_name = 'Indexes';

    private static $description = 'Table of contents';

    private static $icon = 'font-icon-block-content';

    private static $has_many = array(
        'Chapters' => Chapter::class
    );

    private static $inline_editable = false;

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName('Chapters');

        $copycode = '';

        foreach ($this->Chapters() as $node){

            $copycode .= $node->Anchor."\n";

        }
        $copycode .= "\n";
        foreach ($this->Chapters() as $node){

            $copycode .= "class=\"anchor\" id=\"".$node->Anchor."\"\n";

        }

        $CopyCodeField = TextareaField::create('CopyCode', _t('ElementIndex.COPYCODE', "Code for copy"), $copycode);
        $CopyCodeField->setDescription(_t('ElementIndex.COPYCODE_DESCRIPION', "You can copy and paste this code into a notepad file and use it in the source code of the page."));

        if ($this->ID) {

            $ChaptersGridField = GridField::create(
                'Chapters',
                _t('ElementIndex.CHAPTERS', "Chapters"),
                $this->Chapters(),
                GridFieldConfig_RecordEditor::create()
                    ->addComponent(new GridFieldOrderableRows())
            );

            //$field = TextField::create('test', 'test');
            $fields->addFieldToTab('Root.Main', $ChaptersGridField);
            $fields->addFieldToTab('Root.Main', $CopyCodeField);
        }
        return $fields;
    }

    public function getType()
    {
        return 'Index';
    }
}